function formatEvent(event) {
  return event.geometries.map((geometry, index) => {
    const timestamp = new Date(geometry.date).valueOf() || null;
    //date doesnt throw on invalid date strings so use `if`
    if (timestamp) {
      return Object.assign(
        {},
        event,
        {
          id: `${event.id}.${index}`,
          timestamp,
          geometries: [geometry],
          description: event.description || event.title
        }
      );
    }
  });
}

module.exports = formatEvent;