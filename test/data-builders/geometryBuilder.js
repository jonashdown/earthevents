module.exports = function geometryBuilder() {
  const builder = {
    date: "2019-11-12T21:15:00Z",
    type: "Point",
    coordinates: [
      152.93193,
      -26.55603
    ],

    withDate: (date) => {
      builder.date = date;
      return builder;
    }
  }
  
  return builder;
}
