const chai = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const expect = chai.expect;
const rewire = require("rewire");
const appRoot = require('app-root-path');
const fetchDataFromNASA = rewire(`${appRoot}/app/lib/fetchDataFromNasa`);
chai.use(sinonChai);


describe('Fetches Data from NASA', () => {
  const reverts = [];

  afterEach(() => {
    reverts.forEach((revert) => revert());
  });

  it('makes a call to the NASA events api', () => {
    let requestMock = sinon.spy();
    reverts.push(fetchDataFromNASA.__set__('request', requestMock));

    fetchDataFromNASA().then(() => {
      expect(requestMock).to.have.been.calledWith('https://eonet.sci.gsfc.nasa.gov/api/v2.1/events?limit=10');
    });
  });

});