const util = require('util');
const request = require('request');
const NASA_EVENTS_API = 'https://eonet.sci.gsfc.nasa.gov/api/v2.1/events?limit=10'

function fetchDataFromNASA (done) {
  request(NASA_EVENTS_API, (error, response, body) => {
    if (error) {
      done (error);
    } else if (response.statusCode > 299) {
      done(new Error(`${response.statusCode}: ${response.statusMessage}`));
    } else {
      try {
        const data = JSON.parse(body);
        done (null, data);
      } catch (err) {
        done(err);
      }
    }
  });
}

module.exports = util.promisify(fetchDataFromNASA);
