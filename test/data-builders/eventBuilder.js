module.exports = function eventBuilder() {
  const builder = {
    id: 'EONET_4529',
    title: 'Wildfires - San Jose de Maipo, Chile',
    description:'some bad thing has happened',
    link: 'https://eonet.sci.gsfc.nasa.gov/api/v2.1/events/EONET_4529',
    categories: [
      {
        id: 8,
        title: "Wildfires"
      }
    ],
    sources: [
      {
        id: "PDC",
        url: "http://emops.pdc.org/emops/?hazard_id=97737"
      }
    ],
    geometries: [],
    withId: (id) => {
      builder.id = id;
      return builder;
    },
    withTitle: (title) => {
      builder.title = title;
      return builder;
    },
    withGeometry: (geometry) => {
      builder.geometries.push(geometry);
      return builder;
    },
    withDescription: (desc) => {
      builder.description = desc;
      return builder;
    },
  }

  return builder;
}