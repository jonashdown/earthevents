#! /bin/bash

set -ex

docker build -t earthevents --build-arg dynamodbURL=http://$(cat .dynamoDBIP):8000 .
docker run -d -p 3080:3080 earthevents > .earthEventsDocker