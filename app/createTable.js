// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
const appRoot = require('app-root-path');
const schema = require(`${appRoot}/config/dynamoDBSchema.json`)
const dynamodbURL = process.env.dynamodbURL || 'http://localhost:8000';

AWS.config.loadFromPath(`${appRoot}/config/aws.json`);

const dynamoDB = new AWS.DynamoDB({
  endpoint: new AWS.Endpoint(dynamodbURL),
  apiVersion: '2012-08-10'
});

dynamoDB.createTable(schema, function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Table Created", data);
  }
});
