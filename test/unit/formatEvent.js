const chai = require("chai");
const expect = chai.expect;
const rewire = require("rewire");
const appRoot = require('app-root-path');
const formatEvent = rewire(`${appRoot}/app/lib/formatEvent`);
const eventBuilder = require (`${appRoot}/test/data-builders/eventBuilder`);
const geometryBuilder = require (`${appRoot}/test/data-builders/geometryBuilder`);

describe('Format Event', () => {
  it('creates a single event if there is a single geometry', () => {
    const event = eventBuilder().withGeometry(geometryBuilder());
    const result = formatEvent(event);

    expect(result.length).to.equal(1);
  });

  it('adds the date in the geometry to the event as unix timestamp', () => {
    const event = eventBuilder().withGeometry(geometryBuilder());
    const result = formatEvent(event);

    expect(result[0].timestamp).to.be.a('number');
  });

  it('handles errors in the geometry date', () => {
    const event = eventBuilder()
      .withGeometry(
        geometryBuilder().withDate('not a date')
      );
    const result = formatEvent(event);

    expect(result[0]).to.be.an('undefined');
  });

  it('ensures the event description is populated', () => {
    const event = eventBuilder()
      .withTitle('some thing')
      .withGeometry(geometryBuilder())
      .withDescription('');
    
    const result = formatEvent(event);

    expect(result[0].description).to.equal('some thing');
  });

  it('creates multiple events if there are multiple geometries', () => {
    const event = eventBuilder()
      .withGeometry(geometryBuilder().withDate('2019-11-12T21:15:00Z'))
      .withGeometry(geometryBuilder().withDate('2019-11-11T21:24:00Z'))
      .withGeometry(geometryBuilder().withDate('2019-11-12T20:00:00Z'));
    const result = formatEvent(event);

    expect(result.length).to.equal(3); 
  });

  it('creates a different id for each event if there are multiple geometries', () => {
    const event = eventBuilder()
      .withId('TEST_1234')
      .withGeometry(geometryBuilder().withDate('2019-11-12T21:15:00Z'))
      .withGeometry(geometryBuilder().withDate('2019-11-11T21:24:00Z'))
      .withGeometry(geometryBuilder().withDate('2019-11-12T20:00:00Z'));
    const results = formatEvent(event);

    results.forEach((result, index) => {
      expect(result.id).to.equal(`TEST_1234.${index}`);
    });
  });
});