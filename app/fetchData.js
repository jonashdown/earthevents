const fetchDataFromNasa = require('./lib/fetchDataFromNasa');
const formatEvent = require('./lib/formatEvent');
const appRoot = require('app-root-path');
const AWS = require('aws-sdk');
const dynamodbURL = process.env.dynamodbURL || 'http://localhost:8000'

AWS.config.loadFromPath(`${appRoot}/config/aws.json`);

const docClient = new AWS.DynamoDB.DocumentClient({
  endpoint: new AWS.Endpoint(dynamodbURL),
  apiVersion: '2012-08-10'
});

fetchDataFromNasa()
.then((data) => {
  const events = data.events.flatMap(event => formatEvent(event));
  events.forEach((Item) => {
    const params = {
      TableName: 'NasaEvents',
      Item
    };
    docClient.put(params,(err)=>{
      if (err) {
        throw err;
      } else {
        console.log(`Successfully added ${Item.id} - ${Item.title}`);
      }
    });
  });
})
.catch(error => {
  console.error('error: ', error); 
});
