module.exports = function eventBuilder() {
  const builder = {
    title: 'EONET Events',
    description: 'Natural events from EONET.',
    link: 'https://eonet.sci.gsfc.nasa.gov/api/v2.1/events',
    events: [],

    withEvent: (event) => {
      builder.events.push(event);
      return builder;
    }
  }

  return builder;
}