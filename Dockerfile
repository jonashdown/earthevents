FROM node:12

ARG dynamodbURL
ENV dynamodbURL ${dynamodbURL}
EXPOSE 3080

RUN mkdir -p /root/earthevents

WORKDIR /root/earthevents

ADD package.json .
RUN npm install

ADD config ./config
ADD app ./app

CMD npm start