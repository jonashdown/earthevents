# Earth Events

Calls the NASA Earth Events API to get a sample of random events (https://eonet.sci.gsfc.nasa.gov/docs/v2.1) on start-up. These are stored in a local dynamodb running in docker on port `8000`

The app listens on port `3080` and provides the following endpoints:

`/` - just a status endpoint.
`/events/by/id` - sorts events by id.
`/events/by/date`, `/events/by/timestamp` - sorts events by date.
`/events/by/title` - sorts events by title

## To start the app
```npm install```

```npm run start:local``` - this brings up and provisions and populates the dynamodb docker image, before starting the server.

*N.B* do not use `npm start` to start the app. 

## To start the app in Docker
```npm run start:docker``` - this brings up and provisions and populates the dynamodb docker image, before starting the server in a docker image.


## To stop the app
```npm stop``` - this tears down any docker image associated with the app.

## To run tests
```npm test``` - n.b there are only unit tests at this time.

## Required software 
- [docker](https://www.docker.com/get-started)
- [node 12](https://nodejs.org/en/download/)
