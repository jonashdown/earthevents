const AWS = require('aws-sdk');
const appRoot = require('app-root-path');
const express = require('express');
const dynamodbURL = process.env.dynamodbURL || 'http://localhost:8000';

AWS.config.loadFromPath(`${appRoot}/config/aws.json`);

const docClient = new AWS.DynamoDB.DocumentClient({
  endpoint: new AWS.Endpoint(dynamodbURL),
  apiVersion: '2012-08-10'
});

app = express();

app.get('/',(req,res) => {
  res.send('ok');
});

app.get('/events/by/:attribute', (req, res) => {
  const params = {
    TableName: 'NasaEvents',
    Limit: 10,
  };
  let attribute = req.params.attribute;

  if (!['title', 'id', 'timestamp', 'date'].includes(attribute)){
    res.status(404).json({badAttribute: attribute});
  }
  if (attribute === 'date') {
    attribute = 'timestamp';
  }
  docClient.scan(params, (err, data) => {
    if (err) {
      res.err(err);
    } else {
      res.json({events: data.Items.sort((event1, event2) => {
        if (event1[attribute] < event2[attribute]) {
          return -1;
        }
        if (event1[attribute] > event2[attribute]) {
          return 1;
        }
        return 0;
      })})
    }
  });

});

console.log('server started on port 3080');
app.listen(3080);
