#! /bin/bash
set -ex

docker run -d --rm -p 8000:8000 amazon/dynamodb-local > .dynamoDBDocker
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(cat .dynamoDBDocker) > .dynamoDBIP
